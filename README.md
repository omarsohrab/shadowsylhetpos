## Shadow sylhet pos_project

A Point of Sale System Using Laravel

## Installation

1. Clone the repo
2. Change directory: cd into project file
3. Install Laravel: composer install
4. Change your database settings in config/database.php
5. Migrate your database: php artisan migrate
6. Make user
7. View your application in browser

## License
You are free to use it in personal and commercial projects. The code can be forked and modified, but the original copyright author should always be included. 